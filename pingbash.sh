#!/bin/bash

if `[ "$1" == "-h" ]`
then
  echo "usage: ./pingsh.sh [network]"
  echo "example: ./pingbash.sh 192.168.1"

else
  for host in seq {1..254};do
    ping -c 1 $1.$host | grep "64 bytes" | cut -d " " -f4 | sed "s/.$//"
  done
  
fi

