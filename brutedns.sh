#!/bin/bash
if `[ "$1" == "-h" ]`
then
  echo "usage: ./brutedns.sh [target] [wordlist]"
  echo "example: ./brutedns.sh target.com dns.txt"
else 
  for i in $(cat $2);do
    host $i.$1 | grep "has address" | sort -u
  done
fi
