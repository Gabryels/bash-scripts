#!/bin/bash
if `[ "$1" == "-h" ]`
then
  echo "Usage: ./brutesh.sh [target] [wordlist]"
  echo "Example: ./brutesh.sh http://target.com/ wordlist.txt"
else
  for directorie in $(cat $2);do
    CODE=$(curl -s -o /dev/null -w "%{http_code}" $1/$directorie)
    if `[ "$CODE" == "200" ]`
    then
      echo "==> $1/$directorie ::: 200 OK"
    fi
  done
fi